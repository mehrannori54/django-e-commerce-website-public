# Generated by Django 4.1.3 on 2023-04-23 20:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('product_module', '0010_alter_productspecification_product_type_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productspecificationvalue',
            name='product',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='product_specification_value', to='product_module.product'),
        ),
        migrations.AlterField(
            model_name='productspecificationvalue',
            name='specification',
            field=models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to='product_module.productspecification'),
        ),
    ]
