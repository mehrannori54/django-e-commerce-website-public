# Generated by Django 4.1.3 on 2023-01-01 17:27

from django.db import migrations
import django.db.models.deletion
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        ('product_module', '0005_alter_productgallery_alt_text'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productcategory',
            name='parent',
            field=mptt.fields.TreeForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='children', to='product_module.productcategory', verbose_name='دسته بندی مادر'),
        ),
    ]
