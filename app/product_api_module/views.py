from rest_framework import generics, permissions
from .serializers import ProductDetailsSerializer, ProductImagesSerializer, PublicProductSerializer
from product_module.models import Product, ProductGallery
from rest_framework.permissions import AllowAny
from django.shortcuts import get_object_or_404


# Create your views here.
class ProductList(generics.ListAPIView):
    permission_classes = [AllowAny]
    queryset = Product.objects.filter(is_active=True).all()
    serializer_class = PublicProductSerializer


class ProductDetails(generics.RetrieveAPIView):
    permission_classes = [permissions.AllowAny]
    serializer_class = ProductDetailsSerializer

    def get_object(self, queryset=None, **kwargs):
        slug = self.kwargs.get('slug')
        return get_object_or_404(Product, slug=slug)


""" Concrete View Classes
# CreateAPIView
Used for create-only endpoints.
# ListAPIView
Used for read-only endpoints to represent a collection of model instances.
# RetrieveAPIView
Used for read-only endpoints to represent a single model instance.
# DestroyAPIView
Used for delete-only endpoints for a single model instance.
# UpdateAPIView
Used for update-only endpoints for a single model instance.
# ListCreateAPIView
Used for read-write endpoints to represent a collection of model instances.
RetrieveUpdateAPIView
Used for read or update endpoints to represent a single model instance.
# RetrieveDestroyAPIView
Used for read or delete endpoints to represent a single model instance.
# RetrieveUpdateDestroyAPIView
Used for read-write-delete endpoints to represent a single model instance.
"""
