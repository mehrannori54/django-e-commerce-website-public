from product_module.models import Product, ProductGallery, ProductType, ProductSpecification, ProductSpecificationValue
from rest_framework import serializers


class PublicProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'title', 'slug', 'image', 'image_alt', 'price', 'is_active')


class ProductSpecificationSerializer(serializers.ModelSerializer):
    # product_specification_value = ProductSpecificationValueSerializer(many=True, read_only=True)
    class Meta:
        model = ProductSpecification
        fields = ('name',)


class ProductSpecificationValueSerializer(serializers.ModelSerializer):
    specification = ProductSpecificationSerializer(read_only=True)

    class Meta:
        model = ProductSpecificationValue
        fields = ('specification', 'value')


class ProductImagesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductGallery
        fields = ('image', 'alt_text')


class ProductDetailsSerializer(serializers.ModelSerializer):
    product_images = ProductImagesSerializer(many=True, read_only=True)
    product_specification_value = ProductSpecificationValueSerializer(many=True, read_only=True)

    class Meta:
        model = Product
        fields = (
            'id', 'title', 'slug', 'image', 'image_alt', 'price', 'description', 'product_specification_value',
            'product_images')
