from django.apps import AppConfig


class ProductApiModuleConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'product_api_module'
