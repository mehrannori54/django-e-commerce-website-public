from django.urls import path
from .views import ProductList, ProductDetails

urlpatterns = [
    path('list', ProductList.as_view(), name='api_product_list'),
    path('<slug:slug>', ProductDetails.as_view(), name='api_product_details'),
]
