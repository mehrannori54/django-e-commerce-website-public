from site_module.models import Slider
from rest_framework import serializers


class SliderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Slider
        fields = ('url', 'title', 'url_title', 'image',)
