from django.urls import path
from .views import SliderApiView

urlpatterns = [
    path("", SliderApiView.as_view(), name='api_slider_list')
]
