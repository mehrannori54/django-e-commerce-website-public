from rest_framework import generics
from site_module.models import Slider
from rest_framework.permissions import AllowAny
from .serializers import SliderSerializer


class SliderApiView(generics.ListAPIView):
    queryset = Slider.objects.filter(is_active=True)
    permission_classes = [AllowAny]
    serializer_class = SliderSerializer

