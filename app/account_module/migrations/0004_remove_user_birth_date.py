# Generated by Django 4.1.3 on 2022-11-18 15:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account_module', '0003_user_birth_date'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='birth_date',
        ),
    ]
