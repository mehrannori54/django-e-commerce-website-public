from django.apps import AppConfig


class UserApiModuleConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'user_api_module'
